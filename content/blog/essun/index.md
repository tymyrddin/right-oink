---
title: "Essun reborn"
date: "2022-04-13T15:20:00+00:00"
description: "As Essun, finally she has become more grounded, patient and determined, and she has grown into a powerful capable woman who is enraged at the way the world is."
---

As a child she is proud and hardworking, as a young woman, she is arrogant, enthusiastic, curious, ambitious, and frustrated by a lack of recognition of her qualities, and also by the limits of her abilities. As Essun, finally she has become more grounded, patient and determined, and she has grown into a powerful capable woman who is enraged at the way the world is.

And I quote from the [Futurotopixs Feministxs blog](https://zoiahorn.anarchaserver.org/specfic/2022/04/10/essun-lili-xul/)

_Essun was born 3 times_

_A first time in 2015 in the mind of N.K. Jemisin when she initiated the SF/Fantasy trilogy of “The broken Earth series” with Essun as her heroine, a survivor of the adversity in this future world._

_A second time in December 2020 in the frozen kitchen of Calafou, when the communication team re-installed its internal server and migrated its identity from Omnius to Essun, installing this tangible reality on the mezzanine of this community space._

_Finally, on 30.03.2022, a carnal entity was born, a little post-apocalyptic baby, heir to these imaginary worlds, to this silicium, to the love of her parents._

_Essun Lili Xul will live between France and Spain, between imaginary worlds and alternative realities, we hope to introduce her to you very soon._

![Obelisk gate](./obelisk-gate.png)
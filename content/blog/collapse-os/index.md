---
title: "Collapse OS"
date: "2019-08-30T21:37:00+00:00"
description: "Winter is coming and Collapse OS aims to soften the blow."
---
Winter is coming and [Collapse OS](https://collapseos.org/) aims to soften the blow. It is a z80 kernel and a collection of programs, tools and documentation that allows you to assemble an OS. The goal of the project is to be as self-contained as possible. With a copy of this project, a capable and creative person can build and install Collapse OS without external resources (like the internet) on a machine of her design, built from scavenged parts with low-tech tools.

The project is only relevant if the collapse is of a specific magnitude. A weak-enough collapse (just a few fabs that close down, a few wars here and there, hunger, disease, but people are nevertheless able to maintain current technology levels) and it's useless. A big enough collapse and it's even more useless (who needs microcontrollers when you're running away from cannibals). 

The project has been completed.

[![Scavenger parts](./scavenger-parts.png#center)](https://archive.org/details/gamespeopleplay000bern)
---
title: "Immortality Inc."
date: 2020-08-03T06:26:00+00:00
description: "Would you like to live forever? Some billionaires, already invincible in every other way, have decided that they also deserve not to die."
---

_Enlightenment is ego’s ultimate disappointment. ~ Chögyam Trungpa_

Would you like to live forever? Some billionaires, already invincible in every other way, have decided that they also deserve not to die. Today several biotech companies, fueled by Silicon Valley fortunes, are devoted to “life extension” — or as some put it, to solving “the problem of death.” ~ [The Men Who Want to Live Forever, Dara Horn, New York Times, Jan 2018](https://www.nytimes.com/2018/01/25/opinion/sunday/silicon-valley-immortality.html)

So what if we become immortals?

## Likely effects

* Immortality would probably exacerbate the already destructive carelessness with which we engage with the natural order even more. Death is an essential component of the cycle of life and we would run the very serious risk of (increasing) human overpopulation.
* As our mean lifespan increased, more diseases appeared, because we didn't die before they appeared. We have no idea what will reveal itself, but for certain something will. No boredom for doctors, health and medicine researchers and biologists. I wonder for how long?
* Life seems to pass more quickly as we age and can you imagine what it would be like if, after a couple of thousand years, when there are no new and unique things to experience left and you form no distinct memories anymore? When time starts flying by like that …
* Any society which builds itself upon a system of inequalities (as we seem to have done since the Atheneans put on a play that claimed that if there was no poverty nobody would want to work) will eventually begin to experience severe inequality gaps as time draws on. With immortality, for example available to only a few, we can safely look forward to an endless and chaotic cycle of revolutions, or a perpetual civil war. When available to all, we can still look forward to the same. We will all want to be on top, as there is nothing better to do (besides our never-changing jobs).
* When eternity is available, along with the possibility of experiencing all that life has to offer, as the rich have it, many experiences will seem to lose their value. Why do today what you can do tomorrow. And by the way, what's the meaning of life? But that conversation has become boring too.
* And tedious. I don't know about you, but I imagine even shitting can become a very tedious job after say, a thousand years of shitting. All habits really. Probably enough to want to kill myself.
* Those of us that haven't killed ourselves out of extreme boredom (or others in revolutions and civil war) and are still alive after thousand years of shitting, will be extreme “elders” to the newborns. They know naught, we think we know everything. Great Stagnation. No Change. Ever.
* And we'll not retire. Ever. The system simply can't afford it. And due to the stagnation, the same job. And I thought the thousand years of shitting was bad.
* What have we not tried yet? Becoming a criminal? Violence as act of creation?
* Immortality is just mortality with no foreseeable end. Eventually we will die, and likely our death will be violent.
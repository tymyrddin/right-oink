---
title: "War justification"
date: "2022-02-15T09:36:00+00:00"
description: "The elephant in the room"
---

Around 500 years ago indigenous peoples in the Americas, Oceania, Asia and Africa began to experience change, a kind of change they had not experienced before. Europeans arrived and began to lay claim to their lands and resources, frequently slaughtering their people, including women and children, if they stood in their way. For those who survived, Europeans brought disease and slavery.

Not long after these genocidal patterns began, some European theologians and jurists questioned the legality and morality of the onslaught in writings intended to shape encounters with the peoples of the “New World”, but although they argued that the indigenous were the true owners of their lands, they were also children of their time and space, and their writings were filled with cultural biases, providing conceptual support for colonial patterns by their theories of “just war”. 

![Leviathan](./leviathan.png)

_Just war theory poses that war, while terrible (but less so with the right conduct), is not always the worst option. Important responsibilities, undesirable outcomes, or preventable atrocities may justify war._

 The “just war” tradition can be traced as far back as to Ancient Egypt. In China, in the Warring States period, war was justified only as a last resort and only by the emperor but questioning such a decision was not allowed. The success of a military campaign was considered sufficient proof that the campaign had been righteous. The Indian Hindu epic, the Mahabharata, offers the first written discussions of a “just war” (dharma-yuddha or “righteous war”) and Aristotle introduced the concept and terminology to the Hellenic world.

According to Western legalists, "Just war" is an entirely Western concept and different from the Islamic concept of jihad (holy war), because in Muslim legal theory it is the only type of just war. Rooted in Classical Roman and biblical Hebraic culture and containing both religious and secular elements, just war first coalesced as a coherent Western body of thought and practice during the Middle Ages with Augustine of Hippo and Thomas Aquinas. 

## Legitimisation

Theorists eventually modified the law of nations to reflect, and hence legitimise, a state of affairs that subjugated indigenous peoples. Forgetting the origins of the discipline, theorists described the law of nations, or international law, as concerning itself only with the rights and duties of European and similarly “civilised” states, and as having its source entirely in the positive, consensual acts of those states. It is, according to legal doctrine, binding on states but unenforceable.

_International law: Some of the most influential thinking about war and the international system has come from specialists in international law. All of them postulate that there exists an international society of states that accepts the binding force of some norms of international behaviour._

Killing in war as a matter of course may be inferred from the fact that, as stated by [Thomas Hobbes](https://plato.stanford.edu/entries/hobbes-moral/), _“all laws are silent in the time of war”_. Although this traditional law-suspending power of war has been restricted to a certain degree by modern humanitarian international law, it is still commonly assumed that killing in war, unless and as long as not explicitly forbidden, is permitted and thus does not require any further legitimisation. This in contrast to a “normal” homicide, which requires special justification to be considered lawful. 

 With war effectively legitimised by creating a context in which normal law does not apply, and with an unenforceable “international law”, all it then takes to “justify” an intended war, is to dehumanise these "others" and convince those that might stop your war, or those whose support you need for engaging in that war, by presenting “them” as a threat or as committing atrocities.

I highly recommend watching Wag the Dog and Dr. Strangelove.
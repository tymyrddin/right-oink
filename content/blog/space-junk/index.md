---
title: "Space junk"
date: "2019-12-05T15:24:00+00:00"
description: "Hey, go clean up your mess! NO, not more of that crap!"
---

The space around our planet is not a desolate, empty place, it is filled with millions pieces of man-made debris from old rockets, abandoned satellites and missile shrapnel ([Satellite database](https://www.ucsusa.org/resources/satellite-database)). In the sixties and seventies both the U.S. and the Soviet Union tested a lot of anti-satellite weapons, accidents with booster stages and satellites happen, and discarded bits left over from separations, frozen clouds of water and pieces of paint all remain in orbit.

Earth's orbit is segregated into three distinct regions:

* Low Earth-orbit at 200-2000 km above the surface.
* Medium Earth-orbit, a semi-synchronous orbit at 10,000 to 20,000 km above the surface.
* Geostationary orbit, over 36,000 km high, in which objects can remain aloft for millions of years.

The lower the orbit, the less time the object is likely to remain in space before returning to Earth. In the last five decades, an average of one piece of junk fell to the Earth each day. Most of it burns up in the atmosphere before it ever reaches the surface and nearly everything larger than 10 cm gets fragmented. Those that survive the entry, often fall into water, the ocean making up some 70 percent of the Earth's surface.

## Possible effects

* “Most of it burns up”, yes, but the chemicals it is composed of are then released into the atmosphere. Some composite metals and polymers consume ozone when they burn up, and some chemical reactions produce nitric oxide, which can deplete ozone. Still, the impact of this is negligible compared with other activities of mankind that have a far more negative effect on the ozone.
* If you believe humanity’s fate lies among the stars, forget it. Calculating trajectories that avoid the junk is already a critical part of mission safety. The problem may become exponentially more difficult, as plans exist for an enormous amount of G5 satellites. The Kessler Syndrome - in which a single collision will lead to a huge amount of additional pieces of debris, which will then strike more satellites, ending in a chain reaction - may even come true in a few decades. A theoretical syndrome until now, and one that we do wish to avoid.
* Far mad max type future: Yay! Valuable metal scrap from the early stages of a rocket launch! Can be sold, or used for construction.
---
title: "Poverty myths"
date: 2020-09-13T08:12:00+00:00
description: "The current greatest challenge: “Holding the hands of middle-class people, coddling them, softening my words and critiques in order to ease them oh-so-gently into an understanding that the homeless person screaming profanities at them is suffering from the same system that makes them middle-class.” ~ The Spirit of Poverty"
---

The current greatest challenge: _“Holding the hands of middle-class people, coddling them, softening my words and critiques in order to ease them oh-so-gently into an understanding that the homeless person screaming profanities at them is suffering from the same system that makes them middle-class.”_ ~ [The Spirit of Poverty](https://rhydwildermuth.com/2015/04/14/the-spirit-of-poverty/)

Some myths:

* Poor people did it to themselves and only have themselves to blame. It is the fault of the individual.
* People who are living in poverty are uneducated. Children from poverty have the same opportunities as children who do not live in poverty. They just have to go to school.
* Poverty and health are not linked.
* Job creation and a strong economy will help poor people. Getting a job is the key to avoiding and/or escaping poverty.
* We can’t afford to end poverty. 

## Stigmatisation

This is a world of opportunities available to all. As long as one works hard, he or she can accomplish much, if not everything. Anyone wanting to avoid poverty, can do that. Those who find themselves in poverty, have only themselves to blame. Poverty results from laziness, from making poor decisions, from counterproductive attitudes, from lack of education, etcetera. [Not true](/the-divide), this is blaming of the victims.

When believing that, someone in poverty or homelessness may feel embarrassed and maybe even ashamed. A result of stigma and disgrace. In reality, poverty is mostly caused by loss of jobs, by health emergencies, inadequate schools, low wages, and many other societal and economic factors, all not under their control. Collective action is required to fight poverty, but when a group is stigmatised like the impoverished are, people often disassociates themselves from that group. It's them, not us.

The vast majority of the poor have worked extensively in the past, often still do in their communities (essential services, most likely without getting paid) and will continue to work in the future (paid or not). Whether they remain poor depends on collectively addressing no-wage and low-wage jobs. 

## Education

Most poor do not have an education. Again, not true, and if true, usually due to economic causes.

* Increasingly poor people do have an education, but can still only get underpaid (i.e. earning not enough to feed their family with) jobs.
* There are many barriers to education around the world, which generally boils down to a lack of funds for participation and the necessary travel and books. Extra barriers in many locations are a bias against girls’ education or a specific minority. In 2000, it would have been possible to send every child in the world to school if the world had spent less than 1 percent of what it does on weapons for making that happen. It didn't happen. And when an entire nation lacks in education, they become an untrained workforce for an impoverished nation (and an opportunity for large corporations to gain extra cheap labour).

## Health

* With no easily accessible healthcare of preventable and treatable illnesses like malaria, diarrhoea, and respiratory infections can cause these to be fatal. For women, pregnancy and childbirth can be a death sentence. And when having to travel far distances or when paying for medicine, already vulnerable households can get drained of funds and assets (if any were left to begin with).
* Resilience is low. Climate change creates hunger (and refugees) through drought and flooding and can also create conflict. People living in poverty usually don’t have savings, insurances, food storages, or any other reserves, and when a disaster happens, they turn to selling off “assets” to buy essentials. Assets in this case can include children, which are sold for adoption to Western countries, for prostitution, or for (forced) marriage.
* A particularly nasty reinforcing loop related to health issues is where poverty causes hunger, and hunger maintains poverty. Again, women (and their children) bear the brunt. Malnourishment during pregnancy can lead to wasting or stunting in children with a lifetime of health impacts.
* The ravenous commodification rush defends its right to do so, with weapons in hand (or the air). Conflicts destroy infrastructure ((water and power facilities, buildings and roads). Contaminated water can lead to a host of waterborne diseases, even be life-threatening.
* There are SARS, MERS, Cov-2 (Corona) and other viruses and bacteria lurking about.

## Job creation

Job creation and a strong economy will help poor people. Bullocks. Imagine a time and place without an “economy”. Everyone has to work to put food on the table, to care for the young and old and for the sick, and everyone has time to observe natural phenomena, make a flute and play music, make paintings in caves and pass on stories and traditions. Depending on what needs doing, someone with fitting skills and experience takes the lead, then when it is done, steps down again. An economic domain is a social domain of human practices and transactions. It does not stand alone, and for it to work well it is to benefit all that participate in it.

Our economic system doesn't. Economic activity is spurred by production which uses natural resources, labour and capital.

* Natural resources: This planet offers vast natural wealth that no human created. Or rather offered, for if a natural resource has not yet been arrogated and appropriated, it soon will. The commodification rush is unlikely to stop until there is absolutely nothing left to take.
* Labour: A healthy economy creates jobs that are useful and eliminates jobs that make no sense. Instead, we have essential and useful jobs and bullshit jobs (compared to the social value they create). The first two are no-wage or low-wage and the latter is most likely an overpaid “job creator”. When a state is the “job creator” it is probably just a catchy name for politically motivated social programs to maintain the status quo of inequality.
* Capital: Monetary wealth is not an end in itself, creating huge stockpiles of it is pointless. But capital brings status and power. Both monetary wealth and jobs keep the system going. There is no trade off between them. It is not jobs versus wealth. It is my wealth versus yours in an extortion racket that serves the majority. Your majority is bigger than mine, and with indifference to people's true interests.

## End it?

We can’t afford to end poverty. Yes we can. 

* Abolish “labour markets” (commodification of labour).
* Do not regulate, but cancel “share markets” wholly (as they are no longer connected to reality anyway).
* Do not just stop bailing out banks, but abort commercial banking altogether.
* Reform the central bank to become a service for people (instead of for commercial banks and corporations) and offer all humans a digital central bank account.
* The obvious wish list?

In short, we need another “New Deal”, but internationally. And then think really carefully how to create a healthy postcapitalism for nine generations forward (instead of a dystopian version).

## But we don't

Developing and transition countries spend an average of 1.5 percent of GDP on safety net programs. Europe and Central Asia currently spends the most, with average spending of 2.2 percent of GDP; the Sub-Saharan Africa and Latin America and the Caribbean regions are in the middle of the spending range; and the Middle East and North Africa and South Asia regions spend the least, at 1.0 percent and 0.9 percent, respectively. 

Contrary to political rhetoric, the safety nets are extremely weak and filled with gaping holes. Even in Europe, the safety nets have become weaker over the past 40 years due to privatisation, austerity and other budget cutting measures, in favour of maintaining the economic status quo at all costs. And this is an institutional problem, not individual. Anyone in a position of power to do something will be replaced if they do.

* The oligarchs need more weapons to defend “their right” to their commodification rush, and that is expensive. In other words, costs and conflicts.
* Less access to productive land (often due to land grabs, conflict, overpopulation, and/or climate change) and overexploitation of resources like fish or minerals makes many traditional local livelihoods impossible to maintain, and causes (more) conflict, while conflict drives (more) poverty. Large-scale, long-term violence destroys infrastructure (water and power facilities, buildings and roads) and causes people to flee (often with nothing more than the clothes on their backs).
* Women often bear the brunt, adding a layer of inequality. During periods of war, female-headed households become common, and because women often have difficulty getting well-paying work and are typically excluded from community decision-making, their families are particularly vulnerable. The next generation is likely to grow up angry and vulnerable to manipulators. They will be the most powerful Jedis ever.

Reasons enough to want to stop this. Which brings us back to “Yes we can”. We just need to do it collectively.
---
title: "Bias"
date: "2021-04-17T02:52:00+00:00"
description: "The elephant everybody knows is overt bias, namely attitudes or prejudices that someone has at a conscious level. It is obvious and blatant and allows for some of the awesome games people play."
---

The elephant everybody knows is overt bias, namely attitudes or prejudices that someone has at a conscious level. It is obvious and blatant and allows for some of the awesome games people play. 

The elephant in the room that is less known and seems to hardly be talked about is [unconscious bias](https://www.psychologytoday.com/us/blog/the-media-psychology-effect/201604/mris-reveal-unconscious-bias-in-the-brain), our preferences for or against a person, thing, or group that we hold at an unconscious level. Despite our best intentions, we can have deep-seated resistance to differences like race, gender, or physical characteristics, personality type, sexual orientation, you name it.


## Exploration

We are all children of our time and space, as soon as we wake up. 
Have some fun exploring your own bias with [Project Implicit](https://implicit.harvard.edu/implicit/)! We did!

### The making of ...

Unconscious biases are products of by ourselves generated definitions of what is normal and acceptable, and are shaped by past experiences (including successes and failures of our intentions), our context(s), bubble influences,  and for example, impressions we get from media. Conscious and unconscious biases can contradict each other: Our hidden biases may even (continue to) exist if we have a genuine strong desire to be(come) free of such bias.

### Body language

Hidden biases can reveal themselves in action, especially when a person's efforts to control behaviour consciously flags under stress, distraction, relaxation or competition. Unconscious beliefs and attitudes have been found to be associated with language and certain behaviours such as eye contact, blinking rates and smiles.

## Turtle
Apparently the prejudice and stereotyping turtle is a complex process involving a network of neural structures in multiple regions of the brain. 

![alt](./amygdala-connections.png)

### Prejudice
The subcortical amygdala is located deep within the temporal lobes and responsible for a number of emotional processes that help an organism to define a stimulus and therefore respond accordingly. It focuses our attention and is also involved in the general evaluation of faces (trustworthiness of a known individual as well as first impressions), suggesting it plays a role in social processing. It receives projections (direct input) mostly from the sensory regions of the thalamus and the cortex, but also from several other structures such as the hippocampus and the prefrontal cortex. 

It enables us to respond rapidly to immediate threats in advance of more complex cognitive processing. It can trigger  flight-or-fight and tend-and-fend response, and seems to react to social threats in exactly the same way it reacts to physical threats. Unconscious bias, then, could be called the immediate, reflexive, defensive reaction to "other". Snap judgements, jumping to conclusions of causality based upon even the sketchiest of information, and interpreting tiny bits of input by creating a narrative, most likely flawed, yet highly useful for survival.

### Stereotyping 
Stereotyping could be the cognitive component of social bias, the deliberate conceptual linking of social groups to a particular set of perceived inherent and immutable qualities (sometimes called essentialising). This involves the encoding and storage of stereotypical concepts and their application in judgments and behaviors and employs a different neural network from that of the above mentioned quick survival judgments of prejudice. It includes structures in the cerebral cortex that support conscious thought and semantic memory, testing concepts and detecting complexity, nuance, balance, fairness. This then is probably who we think we are.

## Cognitive effects

[The two work together](https://us.macmillan.com/books/9780374533557) with effects in cognition and behaviour. Meaning, unconscious bias can cause our higher-order thinking to malfunction. 

Prejudices caused by the amygdala system are difficult to work on, but it is not impossible. We can nudge ourselves in the "right" direction using internal and external cognitive cues. Meditation can help a lot and exposing ourselves bit by bit to situations previously perceived as threatening and to more diverse environments, we can counteract the cognitive effects of bias.

![alt](./thinking-fast-and-slow.png)


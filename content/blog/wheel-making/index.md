---
title: Making wheels
date: "2022-03-17T22:38:00-00:00"
description: "Wheels have been used by humans for millenia and serve as associative tools to improve memory functions and train our brain to build more perceptions to encountered problems. Perception is not reality, and by building more perceptions we will perceive that. Some really cool headology basics here."
---

Wheels have been used by humans for millenia and serve as associative tools to improve memory functions and train our brain to build more perceptions to encountered problems. Perception is not reality, and by building more perceptions we will perceive that. An article on request.

Perception acts as a lens through which we view reality. Our perceptions influence how we focus on, process, remember, interpret, understand, synthesize, decide about, and act on reality, and the lens through which we perceive is often warped in the first place by our genetic predispositions, past experiences, prior knowledge, expectations, emotions, preconceived notions, self-interest, and cognitive distortions.

## Why build wheels?

The purpose of building a wheel can be to represent perceptions and explore what moves those, or can be to represent time series of changing perceptions, or both. Related to such purposes are finding and removing blocks to achieving new states (of consciousness, for an application, etc), tracking processes and finding new ways, perceiving what has been ignored previously, or simply interpreting a diary and gaining new insights from that. Wheels are truly multifunctional and can make our personal worlds turn.

## Choosing number of directions

Known wheels typically have 8, 13, 16, or 20 directions. Typically. There is nothing that stops us from using less or more directions, save for that the known wheels are supported by traditional rituals and experiences, which anchor the directions. 

Some traditional rituals are extremely effective and can be helpful for us when translated to rituals that work for us in our context and traditions. Building on millenia of human experience feels like standing on the shoulders of those that came before us with gratitude, and gives a sense of being part of something greater than ourselves. A very good starting point.

I usually use 8-direction wheels with 4 holder directions, and 4 mover directions. Only for scenario planning do I use a 13-direction wheel. 

A wheel is wheels in wheels, plus a wheel has no beginning and no end, and there is always another cycle to begin. This way, as our life moves from one phase to another, we are able to understand deeper levels of teachings around the wheel.

## Language of a wheel

The right brain loves (natural) symbolism, metaphors, music and art. Tones of music and wind in trees, and motifs in flowers and art for example, invoke emotions which serve the right brain hemisphere like the "sensory information" and "facts" do in the left brain. Facts cluster to thoughts, emotions cluster to feelings. Similar, just represented in neurons in different brain halves with different clustering algorithms and a different purpose, so to speak.

If our wheel is to [offer an interface to our right brain, for her to steer us better](/left-hemisphere-dominance/), our wheel has to take that into account. If for example, the wheel communicated associations to the right brain using fruit in the main wheel to mark the directions, and one direction was marked with a strawberry, the right brain may even produce a craving for strawberries when it wants the left brain to investigate a phenomenon in that direction. 

Making a wheel is not just a paper exercise, one has to tie in fitting experiences to the directions chosen. Building on tradition, I use fire to associate with imagination and spirit in the East, water to associate with emotions and senses in the South, earth to associate with reality in the West, and the wind with thought in the North. 

A small note on the West direction is needed here: Reality is. We become a part of it after we die (stop thinking), therefor this direction also associates with great mystery and the largest geological scale, maybe even astronomical scale. Perception is not reality. We can get close with meditiations.

## Building a wheel

The dynamic Universe in our wheel has an order that is based on fundamental relationships between the basic forms that are derived from the primary form: the point. In its singularity and in its sacred creation, this point brings forth everything that exists in the wheel, representing all of creation.

Use the point make a circle around it. Make a cross in the circle and on the intersections with the circle put in the holders Fire, Water, Earth and Water. Make an X, and put in the movers, Identity in the South East, Intuition in the South West, Patterns in the North West and Choreography (or non-linear Process) in the North East. 

![Wheel template](./wheel-template.png#center)

## Common seasonal associations for further anchoring

You can build on common associations to anchor the wheel further. Do be careful which associations you use. These are context and cultural dependent. Summertime looks different everywhere. So check for yourself which associations fit for you and use language your right brain hemisphere loves. 

Consider these an example:

Fire: Spring, the place of new beginnings, the dawn of a new day, birth; Innocence, trust, vulnerability, wonder, and curiosity. 

Water: Summer, a time when everything is amplified, growth, expansion; Awareness, in experience or in understanding.

Earth: Autumn, a time in which we harvest (both metaphorically and in the food we gather), and experience the rains that cleanse and nurture life, A time in which we prepare for returning to the cave of creation; Stillness and quiet, like the nighttime, gratitude.

Wind: Winter, a time to be inside. Those that came before us, in physical form and those in spirit, reside here; giving back or sharing your wisdom with others.

What you gestate in the West is developed in the North, birthed in the East and comes into fullness in the South. You then come full circle to the West again to gather what you gestated, developed, gave birth to and nourished into fullness during the previous seasons of the year. Retrospectives are a form of back propagation.

Contemplate the connections for a while. Gestation may take weeks, even months. Tie in some experiences, like staring in fire, water meditations, smelling earth and/or holding a particularly attractive stone, standing in the wind. And not just like that. Take your time and add experiences, for example by holding a stone while realizing where it comes from and how old it is. Stones are a part of nature. Born by it, they adopted the essence of entity. Especially thrilling is the experience of holding a meteorite in connection to the newest scientific perceptions of the birth of our solar system.

## Wheels in wheels

When your first wheel is sufficiently anchored and you can identify with it, and you will know by playful interjections from your right brain, like tunes popping up in your head, or beutiful images, strophes from poems, or popups associated with the associations you loaded, you can start making some wheels in wheels. 

These wheels in wheels do not have to be correct, they can be adjusted over time (using back propagation).

This is for example, a wheel in the North East (Choreography) depicting directions for learning in multiple directions fitting with the context and purpose of the learning, where learning without mistakes paradoxically means allowing for making mistakes: 

![Mistakes wheel](./mistakes-wheel.png#center)

And this is an example of a West wheel for balancing one-on-one mentoring:

![Balanced choreography](./wheel-balanced-choreography.png#center)
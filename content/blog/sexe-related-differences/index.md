---
title: "Sexe-related headology differences?"
date: "2019-09-24T07:21:00-00:00"
description: "Not found, or maybe somewhere else than where people would like it to be?"
---
Note that these have to do with physiological and anatomical differences, are generalisations, and often do not take into account differences that appear and disappear in cultures with higher or lower levels of gender-related egalitarianism ([and which may actually give us some big surprises](https://www.metadechoc.fr/shocking-12-odile-fillod)).

## Corpus callosum

The corpus callosum and its relation to sexe has been a subject of debate in the scientific and lay communities for over a century and is chock-full with motivated reasoning.

* R. B. Bean suggested in 1906 that “exceptional size of the corpus callosum may mean exceptional intellectual activity” and that there were measurable differences between men and women. He went on to claim differences in the size of the callosum across different races. His research was refuted by F. Mall, the director of the same laboratory.
* Holloway and Utamsing suggested sexe related difference in human brain morphology in 1982, relating to differences in cognitive ability. Based on that, Time published an article in 1992 suggesting that, because the corpus is “often wider in the brains of women than in those of men, it may allow for greater cross-talk between the hemispheres — possibly the basis for women’s intuition".
* Since 1980, 49 studies found no sex difference in the size of the corpus callosum, whether or not account was taken of larger male brain size. A study from 2006 using MRI showed no difference in thickness of the corpus when accounting for the size of the subject.

## Amygdala

Functional and structural differences between male and female amygdalae have been observed. One study showed a different lateralisation of the amygdala in men and women. Enhanced memory was related to enhanced activity of the left, but not the right, amygdala in women (remembering details and stimulating thought), whereas it was related to enhanced activity of the right, but not the left, amygdala in men (taking action). Another study found evidence that on average, women tend to retain stronger memories for emotional events than men.

Stereotypically, women are thought of as emotional and men as logical, but this turns out to be false, and the inverse to be true. Apparently men have a larger part of their brain devoted to emotional responses and a smaller region for logical thinking than women. This makes sense if you consider the energy needed to be vigilant for self-protection. Men are probably more hard wired for competition and dominance. A powerful emotional outburst of anger (reactive aggression), when seen through that lens, is helpful to come out on top during a confrontation.

Reactive aggression appears to be exhibited by all mammalian species. It is part of the mammalian gradated response to threat. Low levels of danger from distant threats induce freezing. Higher levels of danger from closer threats induce attempts to escape the immediate environment. Higher levels of danger still, when the threat is very close and escape is impossible, initiate reactive aggression (closely associated with anger).

## Resources

* [Sex Differences in the Human Corpus Callosum: Myth or Reality?, Bishop and Wahlsten, 1997](http://gormanlab.ucsd.edu/courses/files/psy222/Bishop.pdf)
* [Sex differences in the responses of the human amygdala, Stephan Hamann, 2005](http://languagelog.ldc.upenn.edu/myl/llog/Brizendine/Hamann2005.pdf)


---
title: "Stakeholder problems"
date: 2022-02-23T20:01:00+00:00
description: "More women in the workplace, a policy to support the ironing out of undesired or unfair inequalities. Why does it take so long for organisations to walk their talk, and talk their walk? Well, there are stakeholder (problems) ..."
---

More women in the workplace, policies to support the ironing out of undesired and unfair inequalities - Note the undesired and unfair in that sentence, apparently there are also desired inequalities and fair inequalities. I wonder what those look like? 

Whatever for now. How do we get to a more equal and fair organisation, workplace, market, in general a more mature, and less of a schoolyard and old boys network culture?

Knowing change is hard, the usual approach of executive and senior management is to, with the desired change in mind, do a stakeholder analysis first. Stakeholders are those without whom an organisation would not exist, and all the stakeholders have their seat at a table somewhere. Just keep your eye on developing meaningful relationships to discover expectations, needs, wants and desires. Then work backwards from there while solving problems when they occur. Sounds simple, doesn't it? 

It isn't. The models are flawed, and so are people (with respect to any model). More seriously (and I promise no vampire jokes) ...

## Stake holder analysis

Stakeholder analysis is about the identification of who is impacted by which decisions and/or policy changes. Even if one thinks all "stakeholders" are properly identified (impossible), the analysis is most likely academic, and can hide the fact that there may be no real interest in the policy change or decision, or no real interest in the stakeholders or in the impact on stakeholders of decisions and/or policy changes in the making. It is just another going through the motions of going nowhere fast, and ironically, thereby meeting the expectations on the workfloor, and confirming the status quo.

In strategic models, stakeholders who hold the highest degree of influence in the organisation are identified and inducted into decision-making processes (get people in first, so they can move in others, the perfect infiltration method). Problem is, these stakeholders are acknowledged to the extent that they affect shareholders (for example governmental markets), but the interests of the stakeholders themselves remain secondary. Decisions are made to serve shareholder interests before anyone else’s, a strongly biased process. Another problem is, these infiltrants will have to adapt to and adopt the wizard ways of doing things.

The [magic practised by witches](https://wyrd.tymyrddin.dev/) is vastly different from the [magic of the wizards](https://uu.tymyrddin.dev) of the Unseen University. While the wizard's magic is all flashes and bangs, witches see magic as knowing things other people don't know. This usually boils down to experience, common sense and a brand of psychology known as headology. It is not that they can not do wizard's magic, they just think it silly, and not very useful.

## Stakeholder synthesis

In multi-fiduciary type of stakeholder theories all stakeholders are considered to be (equal) fiduciaries in the organisation. A government may invest money in a market, and its contractors invest time and energy. A problem may occur when some people seem to have reservations about treating those who do not (can not) invest time and/or money in the organisation in the same manner as those who do (can do). Can do, most likely as a result of previous positions in the hierarchy.

First line contractors must receive fair compensations for their work and the risks they take, and subcontractors must also be able to run their own businesses and cooperatives, the concerns of the government must be met, and the (social) media must receive transparency from the organisation as far as is reasonable. 

Often forgotten are external stakeholders such as users, clients, local communities, and environment.

## Making magic

Stakeholders will expect some form of compensation for their contribution towards more equitability. For example, that they can expect better working conditions and/or more diversity within the organisation or market, a better income, or having more choices and/or being more involved in decision-making (aka rising up in the ranks). Expectations can vary between individuals and cooperatives (and individuals within cooperatives). 

Where changes may affect people in a "winning position" and the possibility of (future) "underpay", "less pay" or "less winning" or "having to work in other ways than the by them known ways" is perceived, the result can cause sufficient discomfort. This can be enough for people to switch allegiances or alter their expectations for making the magic happen. More women (or a particular woman) can (increasingly) be perceived as a threat. And there can be backlashes against whatever earlier policy changes already tried.  

And there are some more common problems that can occur.

### Lack of congruence
A lack of congruence can create its own set of stakeholder problems. If some people are focused on getting as many people as possible to use any newly created possibilities, while first line contractors have a mission to focus on the quality of service and on one-on-one relationships, the first will want to do more promotion to get a wider variety of clients and/or subcontractors (perhaps for reducing dependency risks), while _the latter will want to stay focused on outreach to get the right types of projects from the existing market for a few marginalised candidates, and a more gradual change towards a new status quo._

### Stakeholder legitimacy
When a specific stakeholder's activities are not in line with an organisation’s values and norms, there can be a stakeholder legitimacy problem. This type of problem often occurs during a single isolated event that does not align with the organisation's mission. And can cause a backlash.

### Organisational legitimacy
In an organisational legitimacy problem, the organisation or movement as a whole violates the norms and values of its external stakeholders. This can impact the market and the reputation of the organisation/movement, as well as all the other people supported by it and supporting it.

### Differences
The generational gap and differing expectations and motivations can also create problems. Older generations may have seen commitment to an organisation as a long-term relationship, and take part in additional activities out of a sense of duty to the whole. Younger generations may wish to get involved in short-term commitments and could be much more motivated by what they can gain from that experience. 

And not just generational differences may play a role. In a world that is changing rapidly and in which the inequality gap grows and grows (also see [Boots Theory of Socioeconomic Unfairness](https://samvimesbootstheory.com/) by Vimes), people may even have to reinvent themselves and their skills multiple times in a lifetime just to survive (and will still have wet feet). 

[Women have all the advantage there](/mindsets), so maybe best if we let the system run its course and maybe the motivated reasoning becomes so utterly clear that it can no longer be denied. Even if that never happens, best if we do not let our stories be run by stakeholder stories like these, and focus on (even) more resilience and flexibility in our capabilities in all directions.

## On stakeholder modelling

Stakeholder modelling is a tool to explore the interests of an organisation's constituency and can be helpful for detecting

* Unclear concepts of stakeholders
* Potential failures in properly identifying legitimate stakeholders
* A lack of common principles that stakeholders can use to align their interests to.
* The grey and dark elephants in the room
* You name it

And it is only that, a model. Each stakeholder’s approach and commitment will be different. Generalisations invalid.
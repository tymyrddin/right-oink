---
title: "Poverty realities"
date: 2020-09-13T08:12:00+00:00
description: "On the lives of billions of humans across the planet who are paying the price for the exploitation of the globe (done mostly by “developed” countries)"
---

On the lives of billions of humans across the planet who are paying the price for the exploitation of the globe ([done mostly by “developed” countries](https://gitlab.com/tymyrddin/nerrin/-/tree/main/notebooks/poverty)):

## Hunger

Undernourishment is not only a consequence of poverty, it also causes poverty, by being passed on from generation to generation – a particularly vicious circle.

* Malnourishment during pregnancy can lead to wasting or stunting in children with a lifetime of impacts. These children are also born with a weakened immune system and are thus more susceptible to infectious diseases. Physical and mental development is restricted, and the child has more difficulty concentrating and as a result produces poorer school results. Malnourished children are also more prone to developing chronic diseases in adulthood. All of which leads to decreased earning opportunities.
* Climate change creates hunger (and refugees) through drought and flooding.
* Hunger can also create conflict.

## Isolation

Isolation limits opportunity in general. This not only goes for isolated communities (no roads or other infrastructure), but also for women and is used in many communities (and religions) to keep minorities down. 

## Infrastructure

Usually, infrastructure is defined as “the physical components of interrelated systems providing commodities and services essential to enable, sustain, or enhance societal living conditions”. This means structures such as roads, railways, bridges, tunnels, water supply, sewers, electrical grids, telecommunications, hospitals and schools.

* Early warning systems for natural disasters, improved building codes and emergency preparedness strategies can save lives and prevent damages.
* An adequate supply of clean water is essential for agriculture and basic sanitation.
* Healthcare services including immunisations, disease prevention and treatment are essentials.
* Gender equality contributes to family and community development, and is vital to the effort to stop poverty.
* The availability of transportation is important for access to work, education and healthcare.
* Education can be a catalyst to pull families and communities out of the cycle of poverty, but the costs can be too high - tuition, textbooks and transportation, plus the money that children might otherwise earn from working.
* With its effects on agriculture, food supplies and water quality, life and livelihood are on the line for many because of changing precipitation patterns, rising sea levels, higher temperatures and extreme weather.
* Help inflicting based on oblivious generalisations can produce counter-productive results. Asking people what it is they need (first) works best.

Lots of research done in the last decades on the previous decades confirms the obvious: Infrastructural development leads to poverty reduction. Results also show that though infrastructure in general reduces poverty, social infrastructure explains “a higher proportion of the forecast error in poverty indicators relative to physical infrastructure”, suggesting that massive investment in social infrastructure could drastically reduce poverty.

Destruction of infrastructure increases poverty. Another obvious. 

## Access to resources

Less access to productive land (often due to land grabs by rich countries and corporations, conflict, overpopulation, and/or climate change) and overexploitation of resources like fish or minerals makes many traditional local livelihoods impossible to maintain.

## Resilience

People living in poverty usually don’t have savings, insurances, food storages, or any other reserves, and when a disaster happens, they turn to selling off “assets” (if any left) to buy essentials. Assets in this case can include children, which are sold for adoption to Western countries, for prostitution, or for (forced) marriage.
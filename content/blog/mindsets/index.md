---
title: "Mind sets"
date: "2021-07-22T10:24:00+00:00"
description: "A mindset is a belief that orients the way we handle situations — the way we figure out what is going on and what we should do. Some fine headology done in here."
---

Supposedly, our mindsets help us spot opportunities but if they are a fixed mental attitude or disposition they can trap us in self-defeating cycles. In other words, people with a fixed mindset (those who believe that abilities are fixed) are less likely to flourish than those with a flexible mindset (those who believe that abilities can be developed). And this is where roleplay comes in, to understand and empathise with other people's positions.

An exploration is these ideas, categorisations, and beliefs: 

## Motivated reasoning

The emotional stake people have in affirming loyalty to state/institutions/groups/families/gender can shape mental operations.

### Motivated cognition

The end or goal motivates the cognition in the sense that it directs mental operations (sensory perceptions; assessments of weight and credibility of empirical evidence; performance of mathematical or logical computation) that we assume/expect to function independently of that goal or end. This is called [motivated cognition](https://www.lesswrong.com/tag/motivated-reasoning).

Motivated cognition is like a description or characterisation of a process and not an explanation in and of itself. For an explanation, we need to know what the need or goal was that did the motivating (or directing) of the agent’s mental processing and the precise cognitive mechanism or mechanisms through which it operated to generate the goal-supporting perceptions or beliefs. For a neuroscientific explanation and some fun, check out our [bias](/bias) explorations.

Examples of goals or needs that can motivate cognition: a person’s financial or related interests; the need to sustain a positive self-image; the need to protect connections to others with whom someone is intimately connected and on whom someone might well depend for support, in emotional and/or material form. The mechanisms vary also: biased information search; biased assimilation; identity-protective cognition.

### Elephants not excluded

Social psychologists and behavioural economists distinguish between two forms of reasoning: Gut (amygdala), which is fast, intuitive, emotional, and prone to bias based on previous experiences and emotional resonances, and head (prefrontal cortex), which is more deliberate, more reflective, more dispassionate, and (possibly) more accurate. Motivated cognition spans the divide: Needs and goals can unconsciously steer not only rapid gut reactions, but also even more systematic forms of analysis that are thought to be examples of head responses.

Expert scientists are prone to motivated reasoning when they conform the performance of their reflective and deliberate evaluations of evidence to the desire they have to see exciting conclusions vindicated and disfavoured ones rejected.

## Conspirational mindsets

The seeming inability of economic interests to explain who believes what “conspirational issues” could be the motivation for examining the contribution that identity-protective forms of motivated cognition are making.

### Anxiety and/or loss of control?

Uscinski and Parent note in their book American Conspiracy Theories that in laboratory experiments researchers have found that inducing anxiety or loss of control triggers respondents to see non-existent patterns and evoke conspiratorial explanations and that in the real world there is evidence that disasters (earthquakes, economic collapse) and other high-stress situations (job uncertainty, disenfranchisement) prompt people to concoct, embrace, and repeat conspiracy theories. Afaik, this seems not too far from the truth.

### Theoretical refinement ...

_“Much of human history is the story of people constantly on the move and constantly looking for answers, and when those answers are not forthcoming, theories and scenarios are made up to fill that unbearable void of ignorance.”_ ~ An Introduction to Conspiracy Theories

A distinction can be made between conspiracy theorists and conspiracy ideologists. The latter then are the ones that promote absurd conspiracies. Such conspiracy ideologies could continue on forever because they are not based on fact and thus permit endless speculation and wild imagination. A great source of out-of-the-box thinking and entertainment and an awesome distraction from real life disasters and high-stress situations.

Ideologies, like religions, can't be proven wrong (even if they are), and no amount argument will dissuade those who believe in the mythical plot. Like something to hang on to. Plus, there's a group of like-minded people and the accompanying sense of belonging (somewhere) that comes with that.

Meanwhile, let us not ignore that some major events in history have been dominated by conspirators who have indeed manipulated political happenings from behind the scenes. And while some of those may actually be true, we're absolved. It's them doing it to us. Not us doing it to others.

## Apocalyptic mindsets

Apocalypses appear in many stories and accounts of human history. They persist over the ages! 

### First thoughts

Persecution, anxiety, stress and poverty seems to often promote apocalyptic thinking (considered by some a specialisation of the above “conspirational thinking”). The end of the world is near. Again! 

For centuries, doomsters and self-styled prophets have claimed to know about the end of the world, emphasizing that their version of the apocalypse will come true (if we do not do as they say).

How come we are so susceptible? Just some thoughts …

* People love drama, even if they fear the nature of that drama. What could be more exciting than for the “End of the World” to be an event in your lifetime? Why are horror and drama movies so popular?
* One could believe that by voicing concern over a problem, one is getting ahead of that problem and preventing it.
* Feeling smarter, like not being the one to let wool be pulled over ones eyes, feeling more prepared for the worst by “knowing” that “The End Is Near”, even if the “sheep” do not see it coming.
* Media hypes over real and imagined problems. Bubbles may exacerbate.
* Such thinking places a person at the centre of a catastrophic battle of good and evil and therefore makes them feel powerful in a world that otherwise would make them feel small and powerless.
* Free floating anger that the world isn't the way we would like it to be, so come on, interstellar highway! We didn't even protest with the right forms for the Vogons!
* Cognitive errors brought on by a shielded childhood and the later realisation that the world can be a very dangerous place, may also be an ingredient.
* And last but not least: Our awareness of our own impending death. 

Personally, I don't need a spoiler alert. And I prefer to be among the ones leaving early, to avoid the rush.

### Second thoughts

Human beings do not seem to be that good at elevating long-term risks and seemingly remote but real concerns, over more immediate concerns and imaginary endings, especially endings with a strong emotional impact.

And by simply labelling people as conspirationalists, real concerns about climate change, pollution, economic failure, energy shortages and pillaging & plundering of others, and military conflict as a consequences of all of those problems, that could lead to hardship and death on enormous scales in the mid to longer term future, are easily swept under the carpet.

### What if ... ?

What if long ago, human existence depended on a constant state of vigilance, and anxiety was passed on from generation to generation because those most vigilant (most aware of the world around them) lived long enough to procreate (and experience Bings)?

To survive and thrive as a being on this planet has always been, in contemporary times still is, and in the near and far future will likely still be happening in the context of finite resources. And in the case of humans, in our modernist worldview run society, resources are extremely unevenly and unfairly distributed. Those quietly creating the most value get paid the least. On top of that, there's the effects of our lifestyle on Earth, and our ignored dependence on it. But what if it isn't all that bad, and we can change our worldview (and mindsets) to one that might bring a future for next generations? A worthy collective imaginary that can not be commodified?

## Mysogynistic mindsets

Most evidence seems to point toward cultural and environmental influences as being strong factors in misogynistic behaviours and thought patterns.

![alt](./how_it_works.png)

### Thinking fast

Misogyny is highly irrational and based on motivated reasoning, but it seems globally and chronically present in humans. Patriarchal cultures and social organizations, religious orthodoxy, intergroup violence and kinship structures favouring fraternity over relational unity are believed to be the major factors that contribute to the development and propagation of misogyny. 

Indicators:

* Historic referencing to make it seem “normal”.
* Unquestioned and unquestionable continuation of traditions and tendencies to maintain the status quo.
* Consumerism and advertising. Cultures of consumerism and entitlement emphasize potential happiness through ownership of property and imply inherent rights to such happiness, and at the expense of who and what that happens is not considered.
* People who do not feel that their psychosexual needs are being met, maybe attach their desires to women in general. Because of such frustration, these people can try to exert what little control they can over others to feel better. And every person that is not agreeable and in-line with that, is considered a severe threat, and an excuse to take it out on.

### Thinking slow

Some people believe that the thousands of years of paternalistic systems may be due to psychogenic forces. Accounts of misogyny occur in virtually every society in recorded history. Recorded history. Evidence is something which we lack while speaking about the earliest societies and women being oppressed historically.

It could very well be that the basic economic divisions of gender roles (man working outside the home, woman working within the home, male ownership of female reproductive capability), became common with the adoption of agriculture and establishment of settlements. Various other aspects have likely changed over time.

## Utilitarian mindsets

Utilitarianism is a family of consequentialist ethical theories that promotes actions that maximize happiness and well-being for the majority of a (human) population. Unlike other forms of consequentialism, such as egoism and altruism, utilitarianism supposedly considers the interests of all “beings” equally. There is some thinking and discussion about other beings than humans, but err …

* Our entire (western) modern society promotes a utilitarian mindset, a world obsessed with metrics: There seems to be a sense that statistics, facts, and scientific measures can be relied upon, while subjective feelings or inclinations are suspect.
* We live in a world that is, in many ways, well-educated, yet people cannot seem to find something that deeply engages them and seem unable to do anything of apparent value with all the tools so easily available.
* There is little or no sense of value in the journey; it is all about the destination.
* No appreciation for deep thought. Answers come from Google or Wikipedia; information processing is automated and done by computers.
* The only real “work” that humans are left to do is the “truly creative thinking” that only a human mind can come up with. It must be applied toward commercial purposes with financial intent of course.
* A loss of real art in people's lives. Instead, deceptive advertising designs, escapist entertainment plots, and works of quasi-artistic origins.
* Considering that human consciousness is limited, unreliable, and obsolete (compared to Artificial Intelligence), we still do need some people to administer, maintain and repair the machines. The poor bastards.

Are we all floating down the river of life in a blissful happy haze yet?

## Anthropocentric mindsets

Anthropocentrism, is a philosophical viewpoint arguing that human beings are the central or most significant entities in the world. This is a basic belief embedded in many Western religions and philosophies. Anthropocentrism regards humans as separate from and superior to nature and holds that human life has intrinsic value while other entities (including animals, plants, mineral resources, “other” humans even, and so on) are resources that may justifiably be exploited for the benefit of “all” (or “humankind”).

### First thoughts

The roots of anthropocentrism can be found in the Creation story told in the book of Genesis in the Judeo-Christian Bible, in which humans are created in the image of God and are instructed to “subdue” Earth and to “have dominion” over all other living creatures. Aristotle’s Politics and Immanuel Kant’s moral philosophy were also quite instrumental is spreading the meme of humanity’s superiority to nature and an instrumental view of nature.


### Second thoughts

Though we would like to think of ourselves as individuals, we are made up of millions of different cells, particles, and micro-biomes that collaborate and create our (very temporary) existence. Each of us is actually way more unique than previously thought. And we belong to this planet, this planet not to us.

## Ecocentric mindsets

Ecocentrism, a philosophy or perspective that places intrinsic value on all living organisms and their natural environment, regardless of their perceived usefulness or importance to human beings. Ecocentrism as a worldview has been with humanity since we evolved. Many indigenous cultures around the world speak of lore and “natural law” that reflects a more ecocentric view of the world.

Ecologist John Stanley Rowe: _It seems to me that the only promising universal belief-system is ecocentrism, defined as a value-shift from Homo sapiens to planet earth. A scientific rationale backs the value-shift. All organisms are evolved from Earth, sustained by Earth. Therefor Earth, not organism, is the metaphor for Life. Earth not humanity is the Life-center, the creativity-center. Earth is the whole of which we are subservient parts. Such a fundamental philosophy gives ecological awareness and sensitivity an enfolding, material focus._

### Second thoughts

What other mindsets is it teamed up with? It too can be abused. The argument that ordinary people, rather than social structures, are to be blamed for climate change still circulates – and invariably pushes in right-wing directions. A focus on human numbers not only misunderstands the causes of the crisis, it dangerously weakens any movement for real solutions.

## A strong mind

Is a strong mind open and fascinated, a mind that understands change, the transitory passage of thought and the inconsistencies of emotion? Or is it fixed and does it believe in its position, identifying totally and in good faith with every single thought or feeling it experiences?

### Ideologically Turing-capable

The Ideological Turing Test is a concept invented by American economist Bryan Caplan to test whether a political or ideological partisan correctly understands the arguments of his or her intellectual adversaries: the partisan is invited to answer questions or write an essay posing as his or her adversary. If neutral judges cannot tell the difference between the partisan's answers and the answers of the adversary, the candidate is judged to correctly understand the opposing side.

### Credence calibration

Thinking in probabilities (and therefore also in possibilities) is essential to being a forecaster (someone practising the art and science of looking forward), so it is perhaps a skill and tendency worth cultivating on purpose.

### Steel (wo)man

The term “steel man” is used to refer to a position's or argument's improved form. A “straw man” is a misrepresentation of someone else's position or argument that is easy to defeat: a steel man on the other hand, is an improvement of someone else's position or argument that is harder to defeat than their originally stated position or argument. This advance makes it more likely that objections contain less motivated reasoning, making them stronger. Or, we discover our own motivations for holding on to a position or argument.

## 42

If others tell us something we make assumptions, and if they don’t tell us something we make assumptions to fulfil our need to know and to replace the need to communicate. Even if we hear something, and we don’t understand, we make assumptions about what it means and then believe the assumptions. We make all sorts of assumptions because we don’t have the courage to ask questions. What if we were to question everything? Too much. And we know the answer already. 42.

## The five agreements

Not to be seen as magical recipe, Miguel Ruiz initially gave four principles, four agreements, that are easily recognisable as potentially beneficial for self, life and others. Later Ruiz added a fifth agreement using doubt (uncertainty) as a tool to discern the truth. Doubt takes us behind the words we hear to the underlying real message or intent. By being sceptical, we don’t believe every message we hear, and when we don't put our faith in lies, we quickly move beyond emotional drama and feelings of victimisation, if any. The fifth agreement is claimed to be about seeing our own “reality” through the eyes of “truth”, as if we ever could, but like said, the quest for it is never boring.

Do not be mistaken about the challenges and resistance the mind will have to living just one of the agreements:

* Be impeccable with your word: Speak with integrity. Say only what you mean. Avoid using the Word to speak against yourself or to gossip about others. Use the power of your Word in the direction of truth and love.
* Don't take anything personally: Nothing others do is because of you. What others say and do is a projection of their own reality, their own dream. When you are immune to the opinions and actions of others, you won't be the victim of needless suffering.
* Don't make assumptions: Find the courage to ask questions and to express what you really want. Communicate with others as clearly as you can to avoid misunderstandings, sadness and drama. With just this one agreement, you can completely transform your life.
* Always do your best: Your best is going to change from moment to moment; it will be different when you are healthy as opposed to sick. Under any circumstance, simply do your best, and you will avoid self-judgement, self-abuse, and regret.
* Be sceptical, but learn to listen: Don’t believe yourself or anybody else. Use the power of doubt to question everything you hear. Listen to the intent behind the words, and you will better understand the message.

The result of practising these agreements is a better acceptance of ourselves and others. When we change our behaviour we do not change our self, just that, our behaviour. That's hard enough. ;) 

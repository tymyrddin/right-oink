---
title: "Left hemisphere dominance"
date: "2020-10-02T09:02:40+00:00"
description: "All prepare for the systematic society of the future, put together by human will. And may I say, we seem already well underway in constructing such an unremarkable world out of its replaceable parts!"
---

Two hemispheres, with a structural asymmetry. The left hemisphere has been instrumental in all that humankind has achieved. It is a wonderful servant, and a very poor master. The right hemisphere, though it is not dependent on the left hemisphere in the same way that the left is on the right, needs the left hemisphere to achieve its full potential, to become its authentic self. The left hemisphere is dependent on the right hemisphere to ground its world and to be lead back to life, and can be in denial about that. What if we as a society approach everything as if it were a task for the left hemisphere? What if the left hemisphere becomes so dominant that it suppresses the right hemisphere's world altogether? What has happened to the world so far, and to us as humans, through viewing the world increasingly as a mechanism?

An exercise in systems thinking with both hemispheres:

## Abstraction

### Knowledge but not knowledge

* The broader picture, lacking the appearance of clarity and certainty the left hemisphere wants, will likely be substituted with a more narrowly focused and restricted yet very detailed world view. The “bits” and “parts” into which a “thing” can be disassembled will likely be thought of as more important for leading to “knowledge” and “understanding” than the whole (which is in this view just a sum of the parts and nothing more).
* The narrowly focused attention most likely leads to more specialisation and technicising of “knowledge”, which will then increasingly replace building knowledge from information and information gathering, especially when these are gathered through real life observations, exchanges and experiences. Such “knowledge” will be considered more real than “wisdom” (way too nebulous that one; it cannot be grasped, only grokked).
* The left hemisphere can then focus on refinement of the details of its “knowledge” (it is very good at that), increasingly gaining blind spots for what is not clear or certain or cannot be brought to attention in the middle of the visual field.
* That then increases the dismissive attitude towards anything outside of its limited focus, and people with a take on the whole picture will be increasingly marginalised. Knowledge through experience and practical acquisition of embodied skill will appear incomprehensible, or a threat.
* One countermove to this threat would be to replace it with tokens or representations, formal systems evidenced with paper qualifications and certificates, and quantifiable and repeatable processes (increasingly we will not do skill and judgement gained through life experience any more). Real experts (“one who is experienced”) will be replaced with “experts” based only on theory.
* Skills will likely be reduced to algorithmic procedures that can be measured and regulated to make sure they are applied “evenly” and “correctly” by those “qualified”.

### Virtualisation

* The concrete will increasingly be replaced with the theoretical or abstract.
* Leading to humans, human bodies, the material world, music and works of art we make to understand it, becoming more conceptualised and regarded as things.
* The world will become virtualised and our experience of it more and more done through meta-representations.
* Less people will find themselves doing work which requires actual contact with any body or anything in the real world (other than their keyboard).
* Work then, becomes more and more the meta-process of documenting and/or justifying the work itself, at the expense of real work being done in the living world, most likely by marginalised people.

### Effects of virtualisation

* A vast expansion of bureaucracy and systems of abstraction and control using the essential elements of bureaucracy: the necessity of procedures that are known (and in principle knowable); anonymity; organisability; predictability; a concept of justice that is reduced to mere equality; and explicit abstraction.
* Complete loss of a sense of uniqueness.

## Reification

### Mechanisticity

* Increasingly the living would be modelled on the mechanical.
* Which affects how we deal with our bodies, human situations and society at large: how much can it do; how fast can it do it; and with what degree of precision. In the real world changes in scale, speed and precision have an effect on the quality of the experience and in the ways we interact, and can be quite damaging and destructive, but that is ignored.
* Considerations of quantity may replace considerations of quality all together.
* Numbers replace responses to individuals and awareness of context, whether they be people, places, things or circumstances.
* Either/or replaces matters of degree.
* Which increases inflexibility.

### Effects of mechanisticity

* Consciousness changes its nature in a context of technological production and adopts a number of qualities that are manifestations of the world according to the left hemisphere, entrenching that view. For example, bureaucracy would be a product of the left hemisphere and a reinforcement of it in the external world.
* The development of a system that permits things to be reproduced endlessly, and enforces submergence of the individual in its production lines, where “measurability” means the insistence on quantification; “componentiality” means reducing reality to self-contained units of which the components can be analysed, taken apart and put together again; and an “abstract frame of reference” means loss of context.
* Maintaining one's own integrity as a unique, individual subject, will become quite impossible. No more “mystery of being”.
* People in such a society will find it hard to understand higher values, except for those framed in terms of utility. All other values will be ignored or treated with derogation and cynicism.
* Morality issues will at best be judged on the basis of utilitarian calculation (enlightened self-interest).

### Exploitation

* With the impersonal replacing the personal, there would be a focus on material things at the expense of the living.
* Social cohesion between persons, between persons and places, are more and more neglected, in some cases even actively disrupted (social cohesion can not be measured and is therefore incomprehensible and inconvenient).
* Depersonalisation of the relationships by members of society, and vv increases.
* Exploitation rather than co-operation would become, explicitly and implicitly, the default relationship between humans, and between humanity and the rest of the planet. This creates resentment. Especially from those being plundered.
* This resentments is fertile ground for an increase in uniformity and “equality”, not as just one desiring to be balanced with others, but as the ultimate desirable, transcending all others.
* There can be only one, and governments of countries that do not submit to having their resources commodified and plundered for “the good of all”, will be highly suspect and a possible object for intervention and possibly war, with all of the propaganda and increased poverty that that brings.
* We will likely see more and refined categorisation based on socio-economic groups, gender, race, which can feel in competition with or resentful of one another.
* As individualities are ironed out, there will be, by the way, less than fair treatment for marginalised people. Wut? Fair??? Excuse my sarcasm.
* Paranoia and lack of trust would become the pervasive stance within society between individuals, between individuals and groups, between groups, between government (and its bureaucracy) and the people it is supposed to serve, and between governments.
* Note that both government and its bureaucracy do not view themselves as servants of its people, but rather adopt a commodified view of its people. They are the benevolent masters, “helping” others that obviously could not make it to where they are (are not so far evolved).

## Total control

### Governments

* Talk of liberty as an abstract ideal, while curtailing individual liberty.
* Panoptical control would become an end in itself, and CCTV monitoring and interception of private information the norm.
* Which includes introducing DNA databases apparently in response to exceptional threats and circumstances (against which these measures would be largely ineffective), their true aim being the increase of the power of the state and playing down the status and responsibility of the individual (because the left hemisphere believes individuals are simply interchangeable (“equal”) parts of a mechanistic system, a system it needs to control in the interests of efficiency).
* The state would take greater power directly and play down individual responsibility, and subsequently the sense of individual responsibility would also decline.
* Family relationships and skilled roles that transcend what can be quantified and regulated (and most likely depend on a degree of altruism) would become the object of suspicion (the left hemisphere mistakes altruism as a form of self-interest) and a threat.
* The left-hemisphere may even try to damage the trust in such roles, by discrediting them.
* Efforts to bring families and professions under bureaucratic control will likely increase.

### Society

* Accidents and illnesses (out of our control) would be particularly threatening, and are blamed on others whenever possible, death being the ultimate challenge of course. Death becomes a taboo, the ultimate unspeakable, and longevity a business, Immortality Inc.. We start with organ transplants.
* Sex will not be an implicit any more, but will become explicit and everywhere.
* Rationality (superrationality) will replace reasonableness. After one or two generations the word “reasonable” may become unintelligible.
* Complete failure of common sense (way too intuitive, and relies on both hemispheres working together) and autonomy (relies on the right hemisphere).
* Anger and aggressive behaviour increases (no more balance with empathic skills).
* Loss of insight, coupled with an unwillingness to take responsibility, reinforcing tendencies to dangerously unwarranted optimism.
* Rise in intolerance and inflexibility, and an unwillingness to change one's [mind(set)](/mindsets).

### Consumerism

* A lack of a sense of autonomy could easily lead to passive stances.
* Increased passivisation and suggestibility most likely leads to increased “forced utilisation behaviours”.
* And because of less personal responsibility, to notions of “being exposed” to such “forced utilisation behaviours”.
* Lack of will power in the sense of self-control and self-motivation, but not in the sense of acquisitive greed and desire to manipulate.
* If it is there, you must use it, do it. Opportunism, not just by those that chase abstract wealth, think also of internet, smartphones and facebook, used by nearly everybody, including intelligence and law enforcement agencies.
* Hence increased consumerism and panopticon.

### Disenchantment

* Undercutting of the sense of awe or wonder in which spirituality is considered a mere fantasy.
* The right hemisphere being drawn forward by examples of its values, and the left hemisphere being driven forward by a desire for power and control, the latter will likely try to undercut, ridicule, or destroy examples the right hemisphere values in life and art.
* Pathos, the characteristic stance of the right hemisphere, could be shamed and become shameful.
* Discerning meaning and value of life could become hard.
* The resulting boredom can lead to a craving for novelty and stimulation in less than healthy ways.
* Things we would normally see as natural, organically evolving, flowing structure, could seem composed of a succession of frames, a sum of infinite series of “pieces”. This could include the passage of historical, cultural and personal time, organically flowing shapes or forms, and the genesis, development, growth, and decay of all things alive, leading to a loss of sense of uniqueness.
* Repeatability leads to over-familiarity through endless reproduction, and to boredom.

### Culture

* We would become spectators rather than actors in all the comedies the world displays.
* Culture becomes conceptual, without the capacity for eliciting the metaphorical power of its incarnate qualities.
    * Visual art would lack a sense of depth. Distorted and bizarre perspectives could become the norm.
    * Music would be reduced to rhythm. Some would try to ascend it but the harmony and melody would be bleak.
    * Dance would be more solipsistic than communal.
* Words and ideas dominate, but the words would be diffuse, excessive, lacking in concrete referents, clothed in abstraction, with no overall feel for its qualities as metaphor of mind.
* Technical and bureaucratic language, devoid of any richness of meaning or significance, would become abundant and applied to everything in sight.
* Learning from history and traditions would be dismissed.
* The body is a machine with replaceable parts, and nature a heap of resources to be exploited.
* What remains of wild and unrepresented nature, just like nonconformists, not managed or submittal to rational exploitation for science or industry, becomes highly suspect and will most likely be seen as a threat and must be brought under bureaucratic control asap.

## All prepare!

All prepare for the systematic society of the future, put together by human will. And may I say, we seem already well underway in constructing such an unremarkable world out of its replaceable parts! 

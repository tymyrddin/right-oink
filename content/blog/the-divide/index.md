---
title: "The Divide"
date: 2022-02-27T02:52:00+00:00
description: "The richest 10% of the global population currently takes 52% of global income, whereas the poorest half of the population earns 8.5% of it."
---

_“The richest 10% of the global population currently takes 52% of global income, whereas the poorest half of the population earns 8.5% of it.”_ ~ [WORLD INEQUALITY REPORT 2022](https://wir2022.wid.world/)


![The divide](./world_map1.png#center)
_Map making: [Poverty data science notebooks](https://gitlab.com/tymyrddin/nerrin/-/tree/main/notebooks/poverty)_

What is causing this growing divide? We are told that poverty is a natural phenomenon that can be fixed with aid. But in reality poverty doesn’t just exist, it has been created. And most of us people living in cluster 2 (yellow in this map, and known as the "first world"), live with [poverty myths](/poverty-myths) in our heads, and know nothing of [its realities](/poverty-realities).

Poor countries are poor because they are integrated into the global economic system on unequal terms. The aid narrative hides the deep patterns of wealth extraction that cause poverty and inequality in the first place: rigged trade deals, tax evasion, land grabs and the costs associated with climate change. 

[The Divide](https://www.jasonhickel.org/the-divide/) tracks the evolution of this system, from the expeditions of Christopher Columbus in the 1490s to the international debt regime, which has allowed a handful of rich countries to effectively control economic policies in the rest of the world.

![The divide](./divide.png#center)
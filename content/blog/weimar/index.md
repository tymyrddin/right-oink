---
title: "Weimar republic failure 2.0? Nah."
date: "2022-10-27T12:00:00+00:00"
description: "The Weimar Republic has been on people's minds with the results of the US presidential election in 2016 and the ongoing rise of the radical right in Europe. Is there a lesson to be learned from the Weimar experience?"
---

The Weimar Republic has been on people's minds with the results of the US presidential election in 2016 and the ongoing rise of the radical right in Europe. Is there a lesson to be learned from the Weimar experience?

![cartoon by William Crawford Young from July 17, 1931](./1931.png)

## The story as now told

It is 1919. Germany lost the First World War, the Kaiser abdicated, and a left-wing revolt was brutally put down. 
The victorious allies demanded a hard vengeance, territory, and crippled Germany’s industrial power. Germany did not 
fare particularly well in the years following the Fist World War, and it was thrown into troubling economic and social 
disorder.

With guts, a constitution was hashed out in the city of Weimar, and a parliamentary democracy managed to rise out of 
the chaos and violence. The Weimar Constitution included these highlights:

* The German Reich is a Republic.
* The government is made of a president, a chancellor and a parliament (Reichstag).
* Representatives of the people must be elected equally every four years by all men and women over age 20.
* The term of the President is seven years.
* All orders of the President must be endorsed by the Chancellor or a Reich Minister.
* Article 48 allows the President to suspend civil rights and operate independently in an emergency.
* Two legislative bodies (the Reichstag and the Reichsrat) were formed to represent the German people.
* All Germans are equal and have the same civil rights and responsibilities.
* All Germans have the right to freedom of expression. 
* All Germans have the right to peaceful assembly.
* All Germans have the right to freedom of religion; there is no state church.

Its greatest liabilities, a crippled economy and a humiliated nation. Against all odds, the new republic almost 
thrived by the late 1920s. Until the world economic collapse of the Great Depression. 

## Natural humanoid responses

The entire political spectrum had their own "cultural milieus", mobilising not only political parties and newspapers, 
but also beer halls, sports clubs, stores, and knitting circles.

Failing to research and resolve the underlying issues that had led to the Great Depression, Weimar had few defenders 
in the defeated, impoverished, and resentful German people.

## Underlying issues

From its uncertain beginnings to a brief season of success and then a devastating depression, the Weimar Republic 
experienced enough chaos to position Germany for the rise of Adolf Hitler and the Nazi Party. More who, what, where, 
when in [History: The Weimar Republic](https://www.history.com/topics/germany/weimar-republic).

Economists and historians are even now still debating the causes of the Great Depression. While we know what happened, 
we have only theories to explain the reason for the economic collapse. Many articles "out there" can help arm us with 
knowledge of the political events that lead to the Great Depression. Of which, IMHO, notable:

_In 1929, the US, like many other countries at the time, was on the Gold Standard, with the dollar redeemable in 
gold and pegged to its value. After the Wall Street crash, nervous investors began to trade their dollars for gold. 
The Fed then moved to up interest rates to protect the dollar's value. Those high interest rates made it difficult 
for businesses to borrow money that they needed to survive, and many ended up closing their doors instead. A downward 
spiral._ ~ [History: 5 Causes of the Great Depression](https://www.history.com/news/great-depression-causes)

_As the United States experienced declining output and deflation, it ran a trade surplus with other countries because 
Americans were buying fewer imported goods, while American exports were relatively cheap. Such imbalances gave rise 
to significant gold flows from other countries to the US, which in turn threatened to devalue the currencies of the 
countries whose gold reserves had been depleted. Central banks in affected countries attempted to counteract the trade 
imbalance by raising their interest rates too, which had the effect of reducing output and prices and increasing 
unemployment in their countries. The resulting international economic decline, especially in Europe, was nearly as bad 
as that in the US._ ~ [Britannica: Causes of the Great Depression](https://www.britannica.com/story/causes-of-the-great-depression)

Also noteworthy, [Federal Reserve: Financial Market Shocks during the Great Depression](https://www.federalreserve.gov/pubs/feds/2010/201022/index.html)

## Shock doctrine

Using that knowledge as stimulus for further thought and venturing into the (seemingly) unseen: In a crisis 
(whether economic collapse, war, natural disaster, coup, or global pandemic), the population is 
in shock, disoriented, reeling; emergency measures are the order of the day. If fascists play their cards right, 
people will even welcome an authoritarian mode of leadership. To describe the way autocrats and corporations take 
advantage of such a moment, Naomi Klein coined the term "Shock Doctrine" in her 2007 book of the same name.

As a TLDR; [YouTube: ‘The Shock Doctrine’, Naomi Klein (2007) – A Book in Five Minutes no.19](https://www.youtube.com/watch?v=erD_ldCVqmw)

In Naomi Klein's book, The Shock Doctrine: The Rise of Disaster Capitalism, she discusses neoliberalism ideals as a 
theory of economics that transfers control of economic factors from the public to the private sector, cuts public 
expenditure from social services like education and healthcare with intention of reducing government role and rule, 
eliminates the concept of the public good, and replaces it with individual responsibility. I think 
[The Shock Doctrine: Should We Be Worried?](https://exploringyourmind.com/the-shock-doctrine-should-we-be-worried/) 
explains it well too.

In other words, the neoliberal ideology creates economic problems, especially for the poor all over the planet, then 
pressures poor people to find solutions to *their* economic problems.

The "Shock Doctrine" evolved through neoliberalism, and its main purpose is to enhance chaos within the world and use 
the shock as a strategic weapon for taking more control over society, making fear a guide that a neoliberal 
government uses to create its shocks. Naomi Klein addresses that these shocks wear off and in order for such a 
government to have control over its people, they have to create new shocks through the use of media outlets to project 
greater chaos than reality. They enhance the fear factor, and push their "responses" to it, [in every way they can](https://www.theguardian.com/news/2022/oct/25/my-doomed-stand-margaret-thatcher-war-truth-central-office-information).

## Anger and insights

Yet, not all major disturbances are opportunities for right wing, free market ideologues to make hay. Seeing the more 
recent crises through the prism of the shock doctrine could be misleading. The blows that have destabilised the system, 
like the 2008 banking crisis, the Coronavirus and the current recession, aggravated by the war in Ukraine and the 
economical war with Russia and China in general, have all taken place on neoliberalism's watch. They are crises for 
the neoliberal order and the elites that support it. And are largely seen by all others as such. As *their* problem.

*Our* problems are not entirely the same:

* *Left, right, middle, whatever direction, ruling classes will always try and turn crises to their advantage at the expense of others, if we allow it.*
* *Wealth does not trickle down. The wealthy take, globally, causing poverty and war.*
* *Increasing anger emotions at the ruling classes and elites.*
* *Finding ways of keeping all refugees as safe as we can.*
* *How to pay rent, keep the elderly and small children warm this winter, while still affording food on the table.*
* *How to survive effects created by the ruling class and elites, and our own having allowed it.*

That said, the ruling class is human and suffers from the same humanoid reactions. Initially they are thrown into 
turmoil and disarray too. In Europe, governments have turned instinctively to private contractors to react to the 
crises, only to discover this doesnae deliver desired results. The pandemic, for example, has forced politicians and some 
elites to renege on everything they have believed in and said about the economy and thrown money at (some others 
bought islands, yachts, [luxurious bunkers](https://www.theguardian.com/news/2022/sep/04/super-rich-prepper-bunkers-apocalypse-survival-richest-rushkoff), 
shock-necklaces to keep their guards under control, or are trying to escape into space). 

This has split their ranks and opened up debate about privatisation and state spending. Pandemic, war and recession 
are causing tremendous suffering, they are also generating anger and insights.

## Habituation to shock

Although shocks can feel intense, our body will only maintain this state for a short period. If too often too much, 
damages can be long term, with lack of self-esteem, lack of confidence, distrust, and even physical effects: appetite 
changes, upset stomach or nausea, stomach pain and other gastrointestinal distress, muscle aches and pains, insomnia, 
fatigue.

If not for those, and only on occasion, Humans are excellent at coping with such experiences. By understanding what 
is happening to our body and our thinking, it is less of a scary experience. This is a great concept to teach 
ourselves and teenagers about. We will be more prepared to manage emotions in stressful situations, just like we 
once could when we were closer to (our own) nature.

Care to create another manifestation which will drive us in other directions and ways of living?

## Choice/Free will/Randomness

Perhaps there will be [no going back to "business as usual"](https://www.theguardian.com/politics/2019/may/28/a-zombie-party-the-deepening-crisis-of-conservatism). 

Maybe the visible patterns and habituation to shock reduces fears in enough people, and having nothing (or little) 
left to lose, we will think more clearly, and will work in other, more gutsy ways. Like neoliberalism wanted, 
for us to take care of our own problems. We do not need more reactions from rulers and elites only interested 
in "maintaining private regimes of power at the expense of others", and we certainly do *not* want another autocrat 
to use the current chaos and take control.

Respond with heart instead of react only for self-interest. Create another effect, and we can maybe change things 
around.


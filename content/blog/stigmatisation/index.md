---
title: "Stigmatisation in Europe"
date: 2021-07-10T20:47:00-00:00
description: "There are so many people who deal with stigma every day, in every aspect of life. And stigmatisation is a term that fits into many (unexpected) different contexts and perspectives."
---

There are so many people who deal with stigma every day, in every aspect of life. And stigmatisation is a term that fits into many (unexpected) different contexts and perspectives. 

## Sociological perspective

Stigmatisation is often considered taboo and associated with generalised and stereotypical beliefs and prejudice behaviours such as neglecting people with dementia or in wheelchairs during conversations. Self-stigmatisation refers to the individual’s beliefs and experiences of being part of a stigmatised group and experience stigma. Some people can also experience family stigmatisation or stigmatisation by association which usually refers to feelings and experiences of family members and close friends of stigmatised people.

Stigma and discrimination are related but considered not the same. Stigma is defined by Goffman as an "attribute that is deeply discrediting" and that reduces the bearer from "a whole and usual person to a tainted, discounted one". In his view, stigma commonly results from a transformation of the body, blemish of the individual character, or membership of a despised group. He emphasizes the relationship between an attribute and a stereotype. Building upon this definition, Link and Phelan define stigma as "stigma exists when a person is identified by a label that sets the person apart and links the person to undesirable stereotypes that result in unfair treatment and discrimination".

## Example: Neanderthals

The stigmatisation of Neanderthals in scientific circles held for a long time. Now we know they were better adapted to life in Europe at the time, with a significant artistic culture. The Neanderthals deserve our greatest respect, as they managed to survive living in small, isolated clans, sometimes in very harsh climates, for over 200,000 years. We still have to show we can even do that. Oh right, be careful with the word "clan".

## Example: Fylfot

One of the most dangerous symbols to mention in Europe is the [fylfot or swastika](https://en.wikipedia.org/wiki/Swastika#Meaning). Many people do not even know of its other purposes in other parts of the world, let alone of the differences between a left facing and a right facing fylfot, all because of its association with Nazism (then) and right-wing nationalists (now) in Europe. 

So. One lives in Europe. Growing up one has heard many stories and seen documentaries on the evil symbol, and read the body language people were having when speaking about it. People do not make many spelling or grammatical mistakes in body languages. 

And then one learns of its other uses elsewhere. Quite the internal tensions to deal with. And don't mention it, to anybody.

## Example: Terror attacks and right-wing nationalists

Some second-generation immigrants were involved in the terror attacks in major European cities such as in Paris in 2015 and Brussels and Munich in 2016. The attacks increased xenophobia in general and anti-immigrant and anti-Islamic sentiments in particular in Europe. The attacks also increased (at least scholarly) interest in the integration of second-generation immigrants.

In addition to the terror attacks, economic restructuring and growing poverty have also resulted in the [rise of right-wing nationalists in Europe](/weimar) again. 

Studying the stigmatisation of minorities and immigrant groups, and non-aggressive [destigmatisation strategies of minorities](https://duckduckgo.com/?q=destigmatisation+strategies+of+minorities&t=ffnt&ia=web), can be very revealing and, on occasion, uplifting.
---
title: "Planned obsolescence"
date: "2019-08-30T21:08:00+00:00"
description: "The development of products with a planned limited lifespan, a meaningless disposable society"
---
“Planned obsolescence” is the term used to describe the development of products with a planned limited lifespan. It all started in Germany: in the 1920s, people came up with the idea of ​​making products that deliberately break down by themselves at a certain point in time and thus have to be thrown away. At that time, a global corporate cartel made a joint agreement to limit the life of light bulbs, [the Phoebus cartel](https://spectrum.ieee.org/tech-history/dawn-of-electronics/the-great-lightbulb-conspiracy). A light bulb is to break after 1000 hours of use.

And its not just light bulbs anymore. Gradually, engineers are forced to create products that wear out faster to increase consumer demand. Designers following the philosophy of “Built In Obsolescence”, have to ask themselves, “How can a product be designed so that it breaks down quite quickly, but it still leaves customer confidence in the product and manufacturer intact”.

Examples of products in such a meaningless disposable society:

* A washing machine designed to last about two years, before it breaks down outside the guarantee time. Most of the components/parts have been manufactured from quality materials with the exception of some vital parts. Two years after purchase, the washing machine will only need minor inexpensive repairs, and between 4 to 5 years the vitals parts begin to wear out and a replacement machine is required.
* Disposable razor blades with over 90% of the body made of materials like plastic that can be used for days rather than weeks.
* Printer cartridges come with proprietary smart chips on them which disable printing when one of the colours falls to a certain level, even if there's really enough ink to do the job. Some cartridges still contain more than 40% of their ink when they display a low ink warning. The smart chips also discourage refilling or use of third-party ink. Companies assert that it is to save the print-head from damage, but that argument is not really valid if you look at the technologies available today.
* Consumer electronics and Flat TVs contain elkos. Under-designed (weak) capacitors get ruined because of ambient temperatures. Suitable capacitors cost only 1 cent more.
* Whenever a company launches a new product - within an anticipated timeline and with several new features - and deliberately withholds these features from the older models - even though they can be easily enabled with a simple software upgrade.
* Nylon is so strong that it was used in ropes for towing cars and for parachutes in the military and is still used in climbing ropes. Being such a strong polymer, doesn't it strike us as odd that a pair of nylon stocking only lasts to our front door? The garment industry re-manufactured nylon to make the stocking fragile by reducing its quality. The inevitable “laddering” and “tearing” of stockings is a strategy to force women to buy new stocking due to the short durability of the product.
* Rechargeable batteries commonly contained in small electronic devices such as MP3 players, PDAs, and some cell phones (such as the iPhone) are often soldered into the unit, making replacement almost impossible for the average user (some companies such as Apple do offer battery replacement, of course for a considerable fee). Some of these batteries also contain circuits that regulate and limit the number of charging cycles the battery will take before it fails to charge, limiting the useful life of the battery, and consequently the unit itself.

Companies using planned obsolescence and consumers buying into it are adding unnecessary waste to landfills and garbage piles around the world.

To put a stop to planned obsolescence, for each product we have to find alternatives and/or alternative ways that last (much) longer, and both makers and users profit more from. For some that means finding DIY or repair/change solutions and for others supporting a member of a cartel willing to cheat (which will not be too hard but could lead to (local) monopolies).

And some cases will be extremely tricky. For example, “lead-free” soldering in electronic devices, often sold as “environment friendly”, can have tin in it. Over time where pure tin exists, the tin can form small spurs called “tin whiskers”. These tin whiskers can cause a short circuit or other problems, and hence can be part of (planned) obsolescence.
